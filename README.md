# Minifying

### Cài đặt thư viện
Nếu chưa từng cài đặt thư viện thì chạy lệnh `npm install`

### Chạy lệnh `gulp` cùng các tham số

1. `school-admin` hoặc `file-upload`: nếu muốn minify cả hai project thì để trống
2. `--folder=...` : Folder gốc (có dấu __/__ ở cuối) trỏ đến folder `hachium`. Mặc định: `/var/www/html/`

_VD:_

* `gulp school-admin --folder=E:/Hachium/`
* `gulp file-upload`
* `gulp`

### Lưu ý
* Có thể xử lý __cache-busting__ bằng các `function revision`



