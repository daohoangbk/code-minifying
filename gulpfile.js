const {series, parallel, src, dest} = require('gulp');
var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');
var plumber = require('gulp-plumber');

var uglify = require('gulp-uglify');
// const uglify  = require("gulp-uglify-es").default;
var bytediff = require('gulp-bytediff');
var rename = require('gulp-rename');
const babel = require('gulp-babel');
var rev = require('gulp-rev');
var revDel = require('rev-del');
var revCollector = require('gulp-rev-collector');
var args = require('minimist')(process.argv.slice(2));
// var rootFolder = 'E:/Hachium/';
var rootFolder = '/var/www/html/';
if (args['folder']) {
    rootFolder = args['folder'];
}

var listFolder = [
    'file-upload/hachium-upload/',
    // 'school-admin/'
];

function minifyFileUpload() {
    var folder = 'file-upload/hachium-upload/';
    var rootPath = rootFolder + folder;
    return src([rootPath + '**/*.js', '!' + rootPath + 'assets/**/*', '!' + rootPath + 'node_modules/**/*'], {base: './'})
        .pipe(plumber())
        .pipe(babel({
            presets: [
                ['@babel/preset-env', {modules: false}]
            ],
        }))
        .pipe(ngAnnotate({add: true}))
        .pipe(bytediff.start())
        .pipe(uglify())
        .pipe(bytediff.stop())
        // .pipe(dest('./'))
        // .pipe(rev())
        // .pipe(dest('./'))
        // .pipe(rev.manifest({base: rootPath, cwd: rootPath}))
        // .pipe(revDel())
        .pipe(plumber.stop())
        .pipe(dest('./'));
}

function revisionFileUpload() {
    var folder = 'file-upload/hachium-upload/';
    var rootPath = rootFolder + folder;
    return src([rootPath + '**/*.js', '!' + rootPath + 'assets/**/*', '!' + rootPath + 'node_modules/**/*'], {base: rootPath})
        .pipe(plumber())
        .pipe(rev())
        .pipe(dest(rootPath))
        .pipe(rev.manifest())
        .pipe(revDel())
        .pipe(plumber.stop())
        .pipe(dest(rootPath));
}

function revisionCollectFileUpload() {
    var folder = 'file-upload/hachium-upload/';
    var rootPath = rootFolder + folder;
    return src([rootPath + 'rev-manifest.json', rootPath + '*.html'], {base: rootPath})
        .pipe(plumber())
        .pipe(revCollector())
        .pipe(plumber.stop())
        .pipe(dest(rootPath));
}

function minifySchoolAdmin() {
    var folder = 'school-admin/';
    var rootPath = rootFolder + folder;
    return src([rootPath + '**/*.js', '!' + rootPath + 'assets/**/*', '!' + rootPath + 'node_modules/**/*'], {base: './'})
        .pipe(plumber())
        .pipe(babel({
            presets: [
                ['@babel/preset-env', {modules: false}]
            ],
        }))
        .pipe(ngAnnotate({add: true}))
        .pipe(bytediff.start())
        .pipe(uglify())
        .pipe(bytediff.stop())
        // .pipe(dest('./'))
        // .pipe(rev())
        // .pipe(dest('./'))
        // .pipe(rev.manifest({base: rootPath, cwd: rootPath}))
        // .pipe(revDel())
        .pipe(plumber.stop())
        .pipe(dest('./'));
}

function revisionSchoolAdmin() {
    var folder = 'school-admin/';
    var rootPath = rootFolder + folder;
    return src([rootPath + '**/*.js', '!' + rootPath + 'assets/**/*', '!' + rootPath + 'node_modules/**/*'], {base: rootPath})
        .pipe(plumber())
        .pipe(rev())
        .pipe(dest(rootPath))
        .pipe(rev.manifest())
        .pipe(revDel())
        .pipe(plumber.stop())
        .pipe(dest(rootPath));
}

function revisionCollectSchoolAdmin() {
    var folder = 'school-admin/';
    var rootPath = rootFolder + folder;
    return src([rootPath + 'rev-manifest.json', rootPath + '*.html'], {base: rootPath})
        .pipe(plumber())
        .pipe(revCollector())
        .pipe(plumber.stop())
        .pipe(dest(rootPath));
}

// exports.defalut = parallel(series(minifyFileUpload, revisionFileUpload, revisionCollectFileUpload), series(minifySchoolAdmin, revisionSchoolAdmin, revisionCollectSchoolAdmin));
exports.default = parallel(minifyFileUpload, minifySchoolAdmin);
exports['school-admin'] = series(minifySchoolAdmin);
exports['file-upload'] = series(minifyFileUpload);
// exports.build = series(minifyFileUpload, revisionFileUpload, revisionCollectFileUpload);